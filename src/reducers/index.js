import { combineReducers } from 'redux'
import logicQuizSubmission from './logic-quiz-submission'
import logicQuizSubmissionSummary from './logic-quiz-submission-summary'
import logicQuizItem from './logic-quiz-item'
import logicQuiz from './logic-quiz'
import restTimeInSeconds from './rename-time'
import reviewQuiz from './review-quiz'
import logicQuizItemAnswers from './logic-quiz-item-answers'

export default combineReducers({
  logicQuizSubmissionSummary,
  logicQuizSubmission,
  logicQuizItem,
  logicQuiz,
  restTimeInSeconds,
  reviewQuiz,
  logicQuizItemAnswers
})
