import * as types from '../constant/action-type'

export default (state = {}, action) => {
  switch (action.type) {
    case types.GET_TW_LOGIC_QUIZ_SUBMISSION_SUMMARY:
      return Object.assign(action.data)
    default:
      return state
  }
}
