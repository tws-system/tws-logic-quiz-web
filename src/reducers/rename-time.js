import * as types from '../constant/action-type'

export default (state = 0, action) => {
  switch (action.type) {
    case types.UPDATE_RENAME_TIME:
      return action.data
    default:
      return state
  }
}
