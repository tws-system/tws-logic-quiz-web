import * as types from '../constant/action-type'
import {uniqWith, xorWith} from 'lodash'

export default (state = [], action) => {
  switch (action.type) {
    case types.UPDATE_LOGIC_QUIZ_ITEM_ANSWERS:
      const data = [action.data].concat(state).filter(item => item)
      return uniqWith(data, (a, b)=> (a.quizItemId === b.quizItemId))
    case types.SUBMIT_LOGIC_QUIZ_ITEM_ANSWERS:
      return xorWith(state, action.data, (a, b)=> (a.quizItemId === b.quizItemId))
    default:
      return state
  }
}
