import * as types from '../constant/action-type'

export default (state = {}, action) => {
  switch (action.type) {
    case types.GET_REVIEW_QUIZ:
      return Object.assign(action.data)
    default:
      return state
  }
}
