import request from 'superagent'
import * as types from '../constant/action-type'
import {getHeaderFromLocalStorage} from '../constant/authenticaiton'

const getQuiz = (data) => ({
  type: types.GET_TW_LOGIC_QUIZ,
  data
})
const getReviewQuiz = (data) => ({
  type: types.GET_REVIEW_QUIZ,
  data
})
export const loadQuiz = (quizId) => {
  return async dispatch => {
    const response = await request
      .get(`api/tw-logic-quizzes/${quizId}`)
      .set('id', 1)
      .set('token', getHeaderFromLocalStorage('jwt'))
      .set('sessionId', getHeaderFromLocalStorage('sessionId'))

    if (!response.ok) {
      return
    }
    dispatch(getQuiz(response.body))
  }
}

export const loadReviewQuiz = ({assignmentId, quizId}) => {
  return async dispatch => {
    const response = await request
      .get(`logic-to-program/api/v2/assignments/${assignmentId}/quizzes/${quizId}/review`)
      .set('id', 1)
      .set('token', getHeaderFromLocalStorage('jwt'))
      .set('sessionId', getHeaderFromLocalStorage('sessionId'))

    if (!response.ok) {
      return
    }
    dispatch(getReviewQuiz(response.body))
  }
}
export const submitReviewQuiz = ({taskId, assignmentId, quizId}) => {
  return async dispatch => {
    const response = await request
      .post(`logic-to-program/api/v2/review/logicQuizzes`)
      .send({taskId, assignmentId, quizId})
      .set('id', 1)
      .set('token', getHeaderFromLocalStorage('jwt'))
      .set('sessionId', getHeaderFromLocalStorage('sessionId'))

    if (!response.ok) {
      return
    }
    dispatch(loadReviewQuiz({assignmentId, quizId}))
  }
}
