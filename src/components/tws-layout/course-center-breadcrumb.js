import React, { Component } from 'react'
import { Breadcrumb } from 'antd'
import breadcrumbs from '../../constant/breadcrumb'
import { withRouter } from 'react-router-dom'
import UrlPattern from 'url-pattern'

class TwsBreadCrumb extends Component {
  state = {
    breadCrumbs: []
  }

  componentDidMount () {
    const {history, location} = this.props
    history.listen(this.update.bind(this))
    this.update(location)
  }
  isIndex(url) {
    return url.includes("/AppCenter")
  }
  update (location) {
    const currentBreadcrumb = breadcrumbs.find(breadcrumb => {
      const pattern = new UrlPattern(breadcrumb.linkTo)
      return pattern.match(location.pathname) !== null
    })
    const pattern = new UrlPattern(currentBreadcrumb.linkTo)
    const params = pattern.match(location.pathname)
    currentBreadcrumb.breadCrumbs.map((breadcrumbs) => {
      const pattern = new UrlPattern(breadcrumbs.linkTo)
      if (params !== null) {
        breadcrumbs.linkTo = pattern.stringify({
          programId: params.programId,
          taskId: params.taskId,
          studentId: params.studentId,
          assignmentId: params.assignmentId,
          quizId: params.quizId
        })
      }
      return true
    })
    this.setState({
      breadCrumbs: currentBreadcrumb.breadCrumbs
    })
  }

  changePage (item) {
    const studentUrl = 'https://school.thoughtworks.cn/learn/program-center/student/index.html#'
    const url = item.linkTo
    if (this.isIndex(url)) {
      window.location.href = 'https://school.thoughtworks.cn/learn/home/index.html#/app-center'
    }
    else {
      window.location.href = studentUrl + url
    }
  }


  render () {
    return (
      <Breadcrumb style={{margin: '12px 0', fontSize: '14px'}}>
        {
          this.state.breadCrumbs.map((item, key) => {
            return (
              <Breadcrumb.Item key={key}><a
                onClick={this.changePage.bind(this, item)}>
                {item.name}
              </a></Breadcrumb.Item>
            )
          })
        }
      </Breadcrumb>
    )
  }
}

export default withRouter(TwsBreadCrumb)
