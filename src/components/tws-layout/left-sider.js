import React from 'react'

import { Menu, Icon, Divider, Button, Popconfirm } from 'antd'
import { connect } from 'react-redux'
import { get } from 'lodash'
import { loadQuiz, loadReviewQuiz, submitReviewQuiz } from '../../actions/tws-logic-quiz'
import { submitQuizItemAnswer } from '../../actions/tws-logic-quiz-submission'
import * as types from '../../constant/action-type'

const {SubMenu} = Menu

class LeftSider extends React.Component {

  state = {
    interval: null,
    restTimeInSeconds: null,
    isFinish: false
  }

  componentDidMount () {
    const quizId = get(this.props, 'match.params.quizId')
    const assignmentId = get(this.props, 'match.params.assignmentId')
    this.props.loadQuiz(quizId)
    this.props.getReviewQuiz({assignmentId, quizId})
  }

  startTimeBox () {
    if (this.state.interval) {
      return
    }
    let interval = setInterval(() => {
      const restTimeInSeconds = this.getRestTimeInSeconds()
      this.setState({restTimeInSeconds})
    }, 1000)

    this.setState({interval})
  }

  componentDidUpdate () {
    if (!isNaN(this.getRestTimeInSeconds())) {
      this.startTimeBox()
    }
  }

  componentWillUnmount(){
    if (this.state.interval) {
      clearInterval(this.state.interval)
      this.setState({interval:null})
    }
  }

  getRestTimeInSeconds () {
    const currentTime = new Date().getTime() / 1000
    const startTime = new Date(this.props.startTime).getTime() / 1000
    const timeBox = this.props.logicQuiz.timeBoxInMinutes * 60
    if (isNaN(startTime) || isNaN(timeBox) || startTime === 0) {
      return 0
    }
    const restTime = timeBox + startTime - currentTime
    return parseInt(restTime, 10)
  }

  isFinish(){
    return this.state.isFinish || this.props.reviewQuiz.id
  }

  getTimeBoxString () {
    const restTimeInSeconds = this.state.restTimeInSeconds
    if(restTimeInSeconds <= 0 || this.isFinish()){
      this.props.updateRestTimeInSeconds(restTimeInSeconds)
      return '答题结束'
    }

    if (restTimeInSeconds === null) {
      return ' 倒计时： - 分 - 秒'
    }

    const restMinutes = parseInt(restTimeInSeconds / 60, 10)
    const restSeconds = restTimeInSeconds % 60

    return ` 倒计时： ${restMinutes} 分 ${restSeconds} 秒`
  }


  isTimeOver(){
    return this.state.restTimeInSeconds < 0 || this.isFinish()
  }

  submitAnswer(){
    const taskId = get(this.props, 'match.params.taskId')
    const quizId = get(this.props, 'match.params.quizId')
    const assignmentId = get(this.props, 'match.params.assignmentId')
    this.props.submitReviewQuiz({taskId, assignmentId, quizId})
  }

  handleMenuSelect(key){
    const {match, history, logicQuizItemAnswers} = this.props
    const {programId, taskId, assignmentId, quizId} = match.params
    if(logicQuizItemAnswers.length > 0){
      this.props.submitQuizItemAnswer({assignmentId,answers: logicQuizItemAnswers})
    }
    const uri = `/student/program/${programId}/task/${taskId}/assignments/${assignmentId}/logic-quizzes/${quizId}/quiz-items/${key}`
    history.push(uri)
  }

  render () {
    const {quizItems, match} = this.props
    const {quizItemOrderNum} = match.params
    const timeBoxString = this.getTimeBoxString()
    const timeOver = this.isTimeOver()

    return (
      <div style={{height: '100%',  borderRight: '1px solid #e8e8e8'}}>
        <div style={{
          textAlign: 'center'
        }}>
         <p>{timeBoxString}</p>
         <Popconfirm title="提交后将不能在对答案进行修改，确定提交？" okText="是" cancelText="否" onConfirm={this.submitAnswer.bind(this)}>
            <Button type="primary" disabled={timeOver} >结束答题</Button>
          </Popconfirm>
          </div>
        <Menu
          onSelect={({key}) => {
            this.handleMenuSelect(key)
          }}
          mode='inline'
          selectedKeys={[quizItemOrderNum]}
          defaultOpenKeys={['sub1']}
          style={{borderRight:'none'}}
        >
        <Divider/>
          <SubMenu key='sub1' title={<span><Icon type='user'/>题目</span>}>
            {
              quizItems.map((item, key) => {
                const exampleText = (item.id === null) ? " <例题>" : ""
                return (<Menu.Item key={key + 1}>{`第${key + 1}题${exampleText}`}</Menu.Item>)
              })
            }
          </SubMenu>
        </Menu>
      </div>
    )
  }
}

const mapStateToProps = ({logicQuizSubmission, logicQuiz, reviewQuiz, logicQuizItemAnswers}) => ({
  quizItems: logicQuizSubmission.twlogicQuizItemAnswer || [],
  startTime: logicQuizSubmission.startTime,
  logicQuiz,
  reviewQuiz,
  logicQuizItemAnswers
})

const mapDispatchToProps = dispatch => ({
  loadQuiz: async param => dispatch(await loadQuiz(param)),
  updateRestTimeInSeconds: (data) => dispatch({type: types.UPDATE_RENAME_TIME, data}),
  getReviewQuiz: async data  => dispatch(await loadReviewQuiz(data)),
  submitReviewQuiz: async data  => dispatch(await submitReviewQuiz(data)),
  submitQuizItemAnswer: async data  => dispatch(await submitQuizItemAnswer(data)),


})

export default connect(mapStateToProps, mapDispatchToProps)(LeftSider)
