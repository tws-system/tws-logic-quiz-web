import React, { Component } from 'react'
import { Layout } from 'antd';
import LeftSider from './left-sider'
import { Route } from 'react-router-dom'
import LOGO from '../../images/logo-white.png'
import BreadCrumbs from './course-center-breadcrumb'

const { Header, Content, Footer, Sider } = Layout;

export default class TWSLayout extends Component {
  state = {
    collapsed: false,
  };


  toggle = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  }

  render() {
    const currentYear = new Date().getFullYear()

    return (
      <Layout>
        <Header style={headerStyle}>
          <a href='https://school.thoughtworks.cn/learn/home/index.html#/app-center'>
            <img src={LOGO} style={logoStyle} alt='logo'/>
          </a>
        </Header>
        <Content style={{ padding: '0 50px' }}>
          <BreadCrumbs />
          <Layout style={{ padding: '24px 0', background: '#fff' }}>
            <Sider width={200} style={{ background: '#fff' }}>
              <Route path="/student/program/:programId/task/:taskId/assignments/:assignmentId/logic-quizzes/:quizId/quiz-items/:quizItemOrderNum" component={LeftSider} />
            </Sider>
            <Content style={{ padding: '0 24px', minHeight: 280 }}>
              {this.props.children}
            </Content>
          </Layout>
        </Content>
        <Footer className='footer' style={{ textAlign: 'center' }}>
          <span>ThoughtWorks Learning Platform ©{currentYear}</span>|
          <a href='http://www.beian.miit.gov.cn'>陕ICP备13005347号-3</a>
        </Footer>
      </Layout>
    );
  }
}

const headerStyle = {
  background: '#595959',
  padding: '24px 48px',
  height: 'auto',
}
const logoStyle = {
  height : '60px',
  float: 'left'
}
