import React from 'react'
import { connect } from 'react-redux'
import { loadQuizSubmissionSummary } from '../../actions/tws-logic-quiz-submission'
import { loadQuiz } from '../../actions/tws-logic-quiz'
import { Button, Col, Row } from 'antd'

class TWLogicQuiz extends React.Component {
  componentDidMount () {
    const {assignmentId, quizId} = this.props.match.params
    this.props.getSubmissionSummary({assignmentId, quizId})
    this.props.loadQuiz(quizId)
  }

  componentDidUpdate (prevProps) {
    const {status} = this.props.logicQuizSubmissionSummary

    if (status === 'started') {
      const {history, match} = this.props
      history.push(`${match.url}/quiz-items/1`)
    }
  }

  isEmpty () {
    const {status} = this.props.logicQuizSubmissionSummary
    return status === 'empty'
  }

  isLoad () {
    const {status} = this.props.logicQuizSubmissionSummary
    return status !== undefined
  }

  handleButtonClick () {
    const {history, match} = this.props
    history.push(`${match.url}/quiz-items/1`)
  }

  render () {
    if (!this.isLoad()) {
      return <div />
    }
    if (this.isEmpty()) {
      return <div style={{fontSize: '1.2em', lineHeight: 2, margin: '5%'}}>
        <Row>
          <Col span={3}>
            <b>题目类型：</b>
          </Col>
          <Col span={20}>
            逻辑题
          </Col>
        </Row>
        <Row>
          <Col span={3}>
            <b>答题时长：</b>
          </Col>
          <Col span={20}>
            <font color='red'>{this.props.logicQuiz.timeBoxInMinutes} </font>分钟
          </Col>
        </Row>
        <Row>
          <Col span={3}>
            <b>温馨提示：</b>
          </Col>
          <Col span={21}>
            <br />
            <li> 在点击 <b>开始答题</b> 按钮之前请确保你有足够的时间完成该逻辑题，当点击 <b>开始答题</b> 后，系统将开始倒计时⌛️，在倒计时结束之前，时间不会暂停。倒计时结束后将禁止提交答案。Good Luck！</li>
            <li> 如果在倒计时之前完成所有的题目，可以直接点击 <b>结束答题</b> 按钮提交。</li>
          </Col>
        </Row>

        <Button type='primary' onClick={this.handleButtonClick.bind(this)}>开始答题</Button>
      </div>
    }
    return <Button onClick={this.handleButtonClick()} />
  }
}

const mapStateToProps = ({logicQuizSubmissionSummary, logicQuiz}) => {
  return {logicQuizSubmissionSummary, logicQuiz}
}

const mapDispatchToProps = dispatch => ({
  getSubmissionSummary: async param => dispatch(await loadQuizSubmissionSummary(param)),
  loadQuiz: async quizId => dispatch(await loadQuiz(quizId))
})

export default connect(mapStateToProps, mapDispatchToProps)(TWLogicQuiz)
