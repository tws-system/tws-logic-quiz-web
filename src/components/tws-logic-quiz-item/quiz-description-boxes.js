import React from 'react'
import { Row, Col } from 'antd'

const QuizDescriptionBoxes = ({initializedBox}) => {
  if (!initializedBox) {
    return null
  }

  const boxes = JSON.parse(initializedBox)
  return (
    <Row>
      <Col span={3}>
        <div style={dStyle}>编号:</div>
        <div style={dStyle}>数字:</div>
        <div style={dStyle}>指令:</div>
      </Col>
      {
        boxes
          .filter((box, idx) => idx > 0)
          .map((box, idx) => {
            return (
              <Col key={idx} span={2}>
                <div style={numberStyle}>{idx + 1}</div>
                <div style={boxStyle}>{box}</div>
              </Col>
            )
          })
      }
    </Row>
  )
}

const dStyle = {
  fontSize: '16px',
  paddingRight: '10px',
  height: '40px',
  lineHeight: '40px',
  margin: '10px',
  minWidth: '70px'
}

const boxStyle = {
  textAlign: 'center',
  fontSize: '20px',
  height: '40px',
  lineHeight: '40px',
  border: '1px solid #000',
  margin: '10px 5px'
}

const numberStyle = {
  textAlign: 'center',
  fontSize: '20px',
  height: '40px',
  lineHeight: '40px',
  margin: '10px 5px'
}

export default QuizDescriptionBoxes
