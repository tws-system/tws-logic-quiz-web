import React from 'react'

const QuizDescriptionInstructions = ({descriptionZh}) => {
  if (!descriptionZh) {
    return null
  }

  const instructions = JSON.parse(descriptionZh)
  return (
    <div>
      <ol>
        {
          instructions
            .filter((ins) => ins.length > 0)
            .map((ins, idx) => {
              return (
                <li style={{lineHeight:'3'}} key={idx}>{ins}</li>
              )
            })
        }
      </ol>
    </div>
  )
}

export default QuizDescriptionInstructions
