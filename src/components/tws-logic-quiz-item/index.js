import React from 'react'
import { connect } from 'react-redux'
import { loadQuizSubmission } from '../../actions/tws-logic-quiz-submission'
import { Row, Col } from 'antd'
import QuizDescription from './quiz-description'
import QuizChart from './quiz-chart'

class TWLogicQuiz extends React.Component {
  componentDidMount () {
    this.refreshQuizItems()
  }

  componentDidUpdate (prevProps) {
    const prevQuizItemOrderNum = prevProps.match.params.quizItemOrderNum
    const quizItemOrderNum = this.props.match.params.quizItemOrderNum

    if (prevQuizItemOrderNum !== quizItemOrderNum) {
      this.refreshQuizItems()
    }
  }

  refreshQuizItems () {
    const {assignmentId, quizId, quizItemOrderNum} = this.props.match.params
    this.props.getQuizzes({assignmentId, quizId, quizItemOrderNum})
  }

  render () {
    const {logicQuizItem} = this.props
    return (
      <div>
        <Row>
          <Col span={16}>
            <QuizDescription {...logicQuizItem} />
          </Col>
          <Col span={8}>
            <QuizChart {...logicQuizItem} />
          </Col>
        </Row>
      </div>
    )
  }
}

const mapStateToProps = ({logicQuizItem}) => {
  return {logicQuizItem}
}

const mapDispatchToProps = dispatch => ({
  getQuizzes: async param => dispatch(await loadQuizSubmission(param))
})

export default connect(mapStateToProps, mapDispatchToProps)(TWLogicQuiz)
