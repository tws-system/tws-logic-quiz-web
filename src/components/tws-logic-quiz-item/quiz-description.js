import React from 'react'
import QuizDescriptionInstructions from './quiz-description-instructions'
import QuizDescriptionSubmitter from './quiz-description-submitter'
import QuizDescriptionBoxes from './quiz-description-boxes'
import QuizDescriptionExampleTips from './quiz-description-example-tips'

const QuizDescription = (props) => {
  return (
    <div>
      <QuizDescriptionExampleTips {...props} />
      <QuizDescriptionBoxes {...props} />
      <QuizDescriptionInstructions {...props} />
      <QuizDescriptionSubmitter {...props} />
    </div>
  )
}

export default QuizDescription
