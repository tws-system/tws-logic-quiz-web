import React from 'react'

const QuizDescriptionExampleTips = ({isExample}) => {
  if (!isExample) {
    return null
  }
  return (
    <h1 style={h1Style}>此题为例题，无需作答</h1>
  )
}

const h1Style = {
  color: "#C00"
}

export default QuizDescriptionExampleTips
