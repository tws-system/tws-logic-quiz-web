import React from 'react'

const QuizChart = ({chartPath}) => {
  let imgSrc = 'http://www.festival56.com/images/loading.gif'

  if (chartPath) {
    imgSrc = `https://s3.cn-north-1.amazonaws.com.cn/tws-system-logic-puzzle/files/${chartPath}`
  }

  return (
    <div>
      <img alt="chart" src={imgSrc} />
    </div>
  )
}

export default QuizChart
