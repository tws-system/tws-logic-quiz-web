import React, { Component } from 'react'
import { HashRouter as Router, Route} from 'react-router-dom'
import Loadable from 'react-loadable'
import TWSLayout from './tws-layout'

const Loading = () => <div>Loading</div>

const TWSLogicQuizItem = Loadable({
  loader: () => import('./tws-logic-quiz-item'),
  loading: Loading
})

const TWSLogicQuizSubmissionSummary = Loadable({
  loader: () => import('./tws-logic-quiz'),
  loading: Loading
})

export default class App extends Component {
  render () {
    return (
      <Router>
        <TWSLayout>
          <Route exact path='/' component={Loading}/>
          <Route exact path='/student/program/:programId/task/:taskId/assignments/:assignmentId/logic-quizzes/:quizId' component={TWSLogicQuizSubmissionSummary} />
          <Route exact path='/student/program/:programId/task/:taskId/assignments/:assignmentId/logic-quizzes/:quizId/quiz-items/:quizItemOrderNum' component={TWSLogicQuizItem}/>
        </TWSLayout>
      </Router>
    )
  }
}
