export default [
  {
    linkTo: '/student/program/:programId/task/:taskId/assignments/:assignmentId/logic-quizzes/:quizId',
    breadCrumbs: [
      {
        name: '思特沃克学习平台',
        linkTo: '/AppCenter'
      }, {
        name: '训练营中心',
        linkTo: '/'
      }, {
        name: '任务卡列表',
        linkTo: '/program/:programId/task'
      },
      {
        name: '任务卡',
        linkTo: '/program/:programId/task/:taskId'
      }

    ]
  }, {
    linkTo: '/student/program/:programId/task/:taskId/assignments/:assignmentId/logic-quizzes/:quizId/quiz-items/:quizItemOrderNum',
    breadCrumbs: [
      {
        name: '思特沃克学习平台',
        linkTo: '/AppCenter'
      }, {
        name: '训练营中心',
        linkTo: '/'
      }, {
        name: '任务卡列表',
        linkTo: '/program/:programId/task'
      },
      {
        name: '任务卡',
        linkTo: '/program/:programId/task/:taskId'
      }]
  }]
