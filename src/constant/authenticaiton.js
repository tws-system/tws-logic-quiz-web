export const getHeaderFromLocalStorage = (key) => {
  return window.localStorage.getItem(key)
}
export const authenticationFilter = (status) => {
  if (status === 403 || status === 401) {
    window.location.href = '/learn/auth/logout'
  }
}
