export const GET_TW_LOGIC_QUIZ_SUBMISSION_SUMMARY = 'GET_TW_LOGIC_QUIZ_SUBMISSION_SUMMARY'
export const GET_TW_LOGIC_QUIZ_SUBMISSION = 'GET_TW_LOGIC_QUIZ_SUBMISSION'
export const GET_TW_LOGIC_QUIZ_ITEM = 'GET_TW_LOGIC_QUIZ_ITEM'
export const GET_TW_LOGIC_QUIZ = 'GET_TW_LOGIC_QUIZ'
export const LOAD_STATE_LOADING = 'LOAD_STATE_LOADING'
export const LOAD_STATE_SUCCESS = 'LOAD_STATE_SUCCESS'
export const LOAD_STATE_FAIL = 'LOAD_STATE_FAIL'
export const UPDATE_RENAME_TIME = 'UPDATE_RENAME_TIME'
export const GET_REVIEW_QUIZ = 'GET_REVIEW_QUIZ'
export const UPDATE_LOGIC_QUIZ_ITEM_ANSWERS = 'UPDATE_LOGIC_QUIZ_ITEM_ANSWERS'
export const SUBMIT_LOGIC_QUIZ_ITEM_ANSWERS = 'SUBMIT_LOGIC_QUIZ_ITEM_ANSWERS'
