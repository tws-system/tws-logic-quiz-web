def label = 'tws-logic-quiz-web'
def jnlpContainer = 'jnlp'
def nodeContainer = 'node'
def dockerContainer = 'docker'
def pvcPath = '/home/jenkins/.gradle'
podTemplate(label: label, namespace: 'ci',
    containers: [
        containerTemplate(
            name: jnlpContainer,
            namespace: 'ci',
            nodeSelector: 'environment=ci',
            image: 'jenkins/jnlp-slave:3.10-1-alpine'),
        containerTemplate(
            name: nodeContainer,
            namespace: 'ci',
            nodeSelector: 'environment=ci',
            image: 'node:10.10.0', 
            ttyEnabled: true, 
            command: 'cat'),
        containerTemplate(
            name: dockerContainer,
            namespace: 'ci',
            nodeSelector: 'environment=ci',
            image: 'docker:18.06.1-ce',
            ttyEnabled: true,
            command: 'cat')
    ],
    volumes: [
        hostPathVolume(
            hostPath: '/usr/share/zoneinfo/Asia/Shanghai', 
            mountPath: '/etc/localtime'),
        hostPathVolume(
            hostPath: '/var/run/docker.sock',
            mountPath: '/var/run/docker.sock'),
        hostPathVolume(
            hostPath: '/usr/local/bin/kubectl',
            mountPath: '/usr/local/bin/kubectl'),
        persistentVolumeClaim(
            claimName: 'ci-jenkins-slave',
            mountPath: pvcPath)
    ]
) {
    node(label) {
        stage('Checkout repo') {
            git url: 'git@gitee.com:tws-system/tws-logic-quiz-web.git',
                branch: 'master',
                credentialsId: 'git-ssh-key'
        }

        // stage('Sonar analysis') {
        //     container(nodeContainer) {
        //         sh 'ci/sonar-analysis.sh'
        //     }
        // }

        stage('Build web') {
            container(nodeContainer) {
                sh 'ci/build.sh'
            }
        }

        stage('Gen image') {
            container(dockerContainer) {
                withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: 'harbor-credential', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD']]) {
                    sh 'ci/gen-image.sh'
                }
            }

        }

        stage('Gen k8s') {
            container(jnlpContainer) {
                dir('ci'){
                    sh './gen-k8s-yml.sh'
                }
            }
        }

        stage('Save k8s file') {
            dir('ci') {
                def destinationDir = "${pvcPath}/deployments/$JOB_NAME/$BUILD_NUMBER"
                sh "mkdir -p ${destinationDir}"
                sh "cp deployment.production.yml ${destinationDir}"
            }
        }

        stage('Deploy to staging') {
            withCredentials([[$class: 'FileBinding', credentialsId: 'kube_config_file', variable: 'KUBECTL_CONFIG_FILE']]) {
                sh 'cat ci/deployment.staging.yml'
                sh 'kubectl --kubeconfig=$KUBECTL_CONFIG_FILE apply -f ci/deployment.staging.yml'
            }
        }
    }
}